package dev.aspyro.toomanyresources2.item;

import dev.aspyro.toomanyresources2.TooManyResources2;
import dev.aspyro.toomanyresources2.block.ModBlocks;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredRegister;

import java.util.function.Supplier;

public class ModItems {

    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(TooManyResources2.MODID);

    public static final Supplier<BlockItem> BAMIRITE_ORE = ITEMS.registerSimpleBlockItem(ModBlocks.BAMIRITE_ORE);
}
