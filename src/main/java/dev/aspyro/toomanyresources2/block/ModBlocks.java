package dev.aspyro.toomanyresources2.block;

import dev.aspyro.toomanyresources2.TooManyResources2;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.neoforged.neoforge.registries.DeferredBlock;
import net.neoforged.neoforge.registries.DeferredRegister;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

public class ModBlocks {

    public static final DeferredRegister.Blocks BLOCKS = DeferredRegister.createBlocks(TooManyResources2.MODID);

    //BLOCKS is a DeferredRegister.Blocks
    public static final DeferredBlock<Block> BAMIRITE_ORE = BLOCKS.register(
            "bamirite_ore",
            () -> new Block(BlockBehaviour.Properties.of()
                    .destroyTime(1.5f)
                    .explosionResistance(6f)
                    .sound(SoundType.STONE)
                    .lightLevel(state -> 0)
            ));


}
